package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	ivona "github.com/goleh/tts"
	"regexp"
)

type TTS interface {
	GetAudioURL(string, bool) string
}

type IvonaTTS struct{}

var UseIvonaTTS IvonaTTS

func (_ IvonaTTS) GetAudioURL(text string, isFemale bool) string {

	output, err := os.Create("public/audio/say.ogg")
	if err != nil {
		log.Print(err)
		return ""
	}
	defer output.Close()

	lang := ivona.LANG_EN
	if cyrillic, _ := regexp.MatchString("[а-яА-Я]+", text); cyrillic {
		lang = ivona.LANG_RU
	}

	gender := ivona.GENDER_MALE
	if isFemale {
		gender = ivona.GENDER_FEMALE
	}

	err = ivona.NewVoice(lang, gender).Say(strings.NewReader(text), output)
	if err != nil {
		log.Print(err)
		return ""
	}

	return fmt.Sprintf("/audio/say.ogg?t=%n", time.Now().UnixNano())
}
