package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/stretchr/gomniauth"
	gomniauthcommon "github.com/stretchr/gomniauth/common"
	"github.com/stretchr/objx"
)

type ChatUser interface {
	UniqueID() string
	AvatarURL() string
}

type chatUser struct {
	gomniauthcommon.User
	uniqueID string
}

func (u chatUser) UniqueID() string {
	return u.uniqueID
}

type ProtectedHandler interface {
	ServeHTTP(http.ResponseWriter, *http.Request, Map)
}

type AuthHandler struct {
	next ProtectedHandler
}

func (h *AuthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if cookie, err := r.Cookie("auth"); err == http.ErrNoCookie || cookie.Value == "" {
		localRedirect(w, r, "/login.html")
	} else if err != nil {
		log.Println("Auth error", err)
		w.WriteHeader(http.StatusUnauthorized)
	} else {
		h.next.ServeHTTP(w, r, Map(objx.MustFromBase64(cookie.Value)))
	}
}

func MustAuth(handler ProtectedHandler) http.Handler {
	return &AuthHandler{next: handler}
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	segs := strings.Split(r.URL.Path, "/")
	if len(segs) < 4 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	action := segs[2]
	provider, err := gomniauth.Provider(segs[3])
	if err != nil {
		log.Print("Error when trying to get provider", provider, "-", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	switch action {
	case "login":
		loginUrl, err := provider.GetBeginAuthURL(nil, nil)
		if err != nil {
			log.Print("Error why trying to GetBeginAuthUrl for", provider, "-", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("Location", loginUrl)
		w.WriteHeader(http.StatusTemporaryRedirect)
	case "callback":
		creds, err := provider.CompleteAuth(objx.MustFromURLQuery(r.URL.RawQuery))
		if err != nil {
			log.Print("Error when trying to complete auth for", provider, "-", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		user, err := provider.GetUser(creds)
		if err != nil {
			log.Print("Error when trying to get user from", provider, "-", err)
			return
		}
		chatUser := &chatUser{User: user}
		m := md5.New()
		io.WriteString(m, strings.ToLower(user.Email()))
		chatUser.uniqueID = fmt.Sprintf("%x", m.Sum(nil))
		avatarURL, _ := avatars.GetAvatarURL(chatUser)
		gender, _ := Map(user.Data()).GetString("gender")
		authCookieValue := objx.New(map[string]interface{}{
			"user_id":    chatUser.uniqueID,
			"name":       user.Name(),
			"avatar_url": avatarURL,
			"gender":     gender,
		}).MustBase64()
		http.SetCookie(w, &http.Cookie{
			Name:  "auth",
			Value: authCookieValue,
			Path:  "/",
		})
		localRedirect(w, r, "/")
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:   "auth",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	})
	localRedirect(w, r, "/")
}
