package main

import (
	"flag"
	"log"
	"net/http"
	"strings"

	"github.com/stretchr/gomniauth"
	"github.com/stretchr/gomniauth/providers/facebook"
	"github.com/stretchr/gomniauth/providers/google"
)

var avatars Avatar = TryAvatars{
	UseFileSystemAvatar,
	UseAuthAvatar,
	UseGravatar,
}

var tts TTS = UseIvonaTTS

func main() {
	addr := flag.String("addr", ":8008", "The addr of the application")
	flag.Parse()

	gomniauth.SetSecurityKey("bJsUUU)mPXts5ujbUeq>e3")
	gomniauth.WithProviders(
		facebook.New(
			"1669335106680650",
			"9c0a19f5962dbdb5f50503bdfef05964",
			"http://chatts.cf"+*addr+"/auth/callback/facebook"),
		google.New(
			"697930456705-ua6c3r19cnqf443cjo37t70q35haoud8.apps.googleusercontent.com",
			"ao85Jfo-Ghh84TSa3TnDVofx",
			"http://chatts.cf"+*addr+"/auth/callback/google",
		),
	)

	//@todo multiple rooms
	chatRoom := NewFeed()
	var uploader Uploader

	http.Handle("/ws", MustAuth(chatRoom))
	http.Handle("/uploader", MustAuth(uploader))
	http.HandleFunc("/auth/", loginHandler)
	http.HandleFunc("/logout", logoutHandler)
	http.Handle("/avatars/", http.StripPrefix("/avatars/", http.FileServer(http.Dir("./avatars"))))

	go chatRoom.Run()

	log.Print("Listening at", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func localRedirect(w http.ResponseWriter, r *http.Request, url string) {
	w.Header().Set("Location", "//"+strings.Split(r.Host, ":")[0]+url)
	w.WriteHeader(http.StatusTemporaryRedirect)
}
