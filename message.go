package main

import (
	"fmt"
	"strconv"
	"time"
)

type Message struct {
	Name      string    `json:"name"`
	Body      string    `json:"body"`
	Time      Timestamp `json:"time"`
	AvatarURL string    `json:"avatar_url"`
	AudioURL  string    `json:"audio_url"`
}

type Timestamp time.Time

func (t *Timestamp) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprint(time.Time(*t).UnixNano() / 1e6)), nil
}

func (t *Timestamp) UnmarshalJSON(b []byte) error {
	ts, err := strconv.ParseInt(string(b), 10, 0)
	if err != nil {
		return err
	}
	*t = Timestamp(time.Unix(0, ts*1e6))
	return nil
}
