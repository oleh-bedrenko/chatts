package main

import (
	"time"

	"github.com/gorilla/websocket"
	"strings"
)

type client struct {
	socket   *websocket.Conn
	send     chan *Message
	feed     *Feed
	userData Map
}

func (c *client) read() {
	for {
		var msg *Message
		if err := c.socket.ReadJSON(&msg); err == nil {
			msg.Body = strings.TrimSpace(msg.Body)
			if len(msg.Body) > 1000 {
				msg.Body = msg.Body[:999] + "..."
			}
			if msg.Body != "" {
				gender, _ := c.userData.GetString("gender")
				msg.Time = Timestamp(time.Now())
				msg.AudioURL = tts.GetAudioURL(msg.Body, gender == "female")
				msg.Name, _ = c.userData.GetString("name")
				msg.AvatarURL, _ = c.userData.GetString("avatar_url")
				c.feed.Forward <- msg
			}
		} else {
			break
		}
	}
	c.socket.Close()
}

func (c *client) write() {
	for msg := range c.send {
		if err := c.socket.WriteJSON(msg); err != nil {
			break
		}
	}
	c.socket.Close()
}
