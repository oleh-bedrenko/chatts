package main

type Map map[string]interface{}

func (m Map) GetString(key string) (string, bool) {
	if value, ok := m[key]; ok {
		if strValue, ok := value.(string); ok {
			return strValue, true
		}
	}
	return "", false
}
