app.controller("MainCtrl", function ($scope, $window) {
	$scope.msg    = [];
	$scope.toSend = "";
	$scope.menuOpen = $window.innerWidth > 770;
	var conn  = new WebSocket(WS_URL);
	conn.onerror = function(e) { window.location.replace("/login.html")};
	conn.onclose   = function ()  { console.log("DISCONNECTED") };
	conn.onopen    = function ()  { console.log("CONNECTED")    };
	conn.onmessage = function (e) {
		if (typeof e.data === "undefined") return false;
		$scope.$apply(function () {
			$scope.msg.push(JSON.parse(e.data))
		});
		window.scrollTo(0,document.body.scrollHeight);
	};
	$scope.send = function () {
		conn.send(JSON.stringify({body: $scope.toSend}));
		$scope.toSend = "";
	};
});
