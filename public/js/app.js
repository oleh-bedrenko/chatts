var WS_URL  = "ws://chatts.cf:8008/ws";
var API_URL = "http://chatts.cf:8008/";

var app = angular.module("app", ['monospaced.elastic']);

app.directive('ngEnter', function () {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if(event.which === 13 && !event.shiftKey) {
				scope.$apply(function (){
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
});
