app.controller("AuthCtrl", function ($scope) {
	$scope.loginMethods = [
		{title: 'Facebook', url: API_URL + 'auth/login/facebook'},
		{title: 'Google',   url: API_URL + 'auth/login/google'}
	];
	$scope.getLogoutURL = function() {
		return API_URL + "logout";
	}
});