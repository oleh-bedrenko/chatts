package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

const (
	socketBufferSize  = 1024
	messageBufferSize = 256
)

type Feed struct {
	Forward chan *Message
	Join    chan *client
	Leave   chan *client
	Clients map[*client]bool
}

var upgrader = &websocket.Upgrader{
	ReadBufferSize:  socketBufferSize,
	WriteBufferSize: socketBufferSize,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func NewFeed() *Feed {
	return &Feed{
		Forward: make(chan *Message),
		Join:    make(chan *client),
		Leave:   make(chan *client),
		Clients: make(map[*client]bool),
	}
}

func (f *Feed) Run() {
	for {
		select {
		case client := <-f.Join:
			f.Clients[client] = true
		case client := <-f.Leave:
			delete(f.Clients, client)
			close(client.send)
		case msg := <-f.Forward:
			// forward message to all clients
			for client := range f.Clients {
				select {
				case client.send <- msg:
					// message has been sent
				default:
					// failed to send
					delete(f.Clients, client)
					close(client.send)
				}
			}
		}
	}
}

func (f *Feed) ServeHTTP(w http.ResponseWriter, r *http.Request, usr Map) {
	socket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print(err)
		return
	}
	client := &client{
		socket:   socket,
		send:     make(chan *Message, messageBufferSize),
		feed:     f,
		userData: usr,
	}
	f.Join <- client
	defer func() { f.Leave <- client }()
	go client.write()
	client.read()
}
