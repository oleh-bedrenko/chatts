package main

import (
	"github.com/stretchr/objx"
	"io/ioutil"
	"log"
	"net/http"
	"path"
)

type Uploader struct{}

func (_ Uploader) ServeHTTP(w http.ResponseWriter, r *http.Request, usr Map) {
	userID, ok := usr.GetString("user_id")
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	file, header, err := r.FormFile("avatar_file")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//@TODO LIMIT AVATAR SIZE

	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	filename := path.Join("public/avatars", userID+path.Ext(header.Filename))
	err = ioutil.WriteFile(filename, data, 0777)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	usr["avatar_url"] = "/avatars/" + userID + path.Ext(header.Filename)
	http.SetCookie(w, &http.Cookie{
		Name:  "auth",
		Value: objx.Map(usr).MustBase64(),
		Path:  "/",
	})
	localRedirect(w, r, "/")
}
